//copyright 2016 Topgyal Gurung All Rights Reserved.
// Author: Topgyal Gurung
// Date:4/25/2016
// Contact:topgyaltsering3@gmail.com
//Description: Tic Tac Toe Project

class Game
{
private:
    const char X = 'X';
    const char O = 'O';


public:
    char square[9] = {'1','2','3','4','5','6','7','8','9'};
    
    int Mode;
    int ChooseWhoBeX;
    int turn;
    int input;
    
    char mark;
    void reset();
    void display();
    void WhoGoFirst();
    int getMove();
    bool checkwin();
    
    bool isFull();
    void switch_turn();
    void go(int);
    char GetTurn();
    bool isFree(int);
    void ChooseMode();

    int m;
    int getMoveByAI();
    int possWin();

};
