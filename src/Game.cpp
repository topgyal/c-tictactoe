//copyright 2016 Topgyal Gurung All Rights Reserved.
// Author: Topgyal Gurung
// Date:4/25/2016
// Contact:topgyaltsering3@gmail.com
//Description: Tic Tac Toe Project

#include "Game.h"
#include <iostream>
#include <cstdlib>
using namespace std;

void Game::ChooseMode()
{
    cout << "1: To play with another person\n"
    << "2: To play with the computer\n"
    << "Please choose a mode to play: ";
    cin >> Mode;

    while (Mode!=1 && Mode!=2)
    {
        cout << "Invalid input! Try again" <<"\n\n";
        ChooseMode();
    }

}


void Game::display()
{
    cout << endl;

    if (Mode==1)
        cout << "Player 1 = X    Player 2 = O" << "\n\n" ;


    else if (Mode==2)
    {
        if (ChooseWhoBeX==1)
            cout << "Player = X    Computer = O" << "\n\n" ;

        else if (ChooseWhoBeX==2)
            cout << "Computer = X    Player = O" << "\n\n";
    }

    cout << "     |     |     " << endl;
    cout << "  " << square[0] << "  |  " << square[1] << "  |  " << square[2] << endl;

    cout << "_____|_____|_____" << endl;
    cout << "     |     |     " << endl;

    cout << "  " << square[3] << "  |  " << square[4] << "  |  " << square[5] << endl;

    cout << "_____|_____|_____" << endl;
    cout << "     |     |     " << endl;

    cout << "  " << square[6] << "  |  " << square[7] << "  |  " << square[8] << endl;

    cout << "     |     |     " << "\n" << endl;
}


void Game::WhoGoFirst()
{
    if (Mode==1) turn = 1;

    else if (Mode==2)
//play with computer
    {
        cout << "\n1: You go first as player X\n"
             << "2: The computer go first as player X\n"
             << "Please make you choice: ";
        cin >> ChooseWhoBeX;

        while (ChooseWhoBeX!=1 && ChooseWhoBeX!=2)
        {
            cout << "Invalid input! Try again" <<"\n\n";
            WhoGoFirst();
        }
        turn = 1;
    }

    else WhoGoFirst();
    //If any input number is invalid, ask user to input again.
}


void Game::switch_turn()
{
    turn++;
}


char Game::GetTurn()
{
    if (turn%2==1) return X;
    else return O;
}


int Game::getMove()
{
    mark = GetTurn();

    if (Mode==1)
    //Play with another player
    {
        if (mark==X)
        {
            cout<<"Player 1, enter your position for X: ";
            cin >> input;
            cout << endl;
            return input;
        }

        else if(mark==O)
        {
            cout<<"Player 2, enter your position for O: ";
            cin>>input;
            cout<<endl;
            return input;
        }
    }

    if (Mode==2)
    //play with computer
    {
        if (ChooseWhoBeX==1)
        //you go first as X
        {
            if (mark==X)
            {
                cout << "It's your turn. Enter your position for X: ";
                cin >> input;
                cout << endl;
                return input;
            }
            else if (mark==O)
            {
                cout << "It's the computer's turn. She is making her move...\n" <<endl;
                input =getMoveByAI();
                return input;
            }
        }

        if (ChooseWhoBeX==2)
            
    //the computer go first as X
        {
            if (mark==X)
            {
                cout << "It's the computer's turn. She is making her move...";
                input =getMoveByAI();
                cout << endl;
                return input;
            }
            else if (mark==O)
            {
                cout << "It's your turn. Enter your position for O: ";
                cin >> input;
                cout << endl;
                return input;
            }
        }
    }
}


void Game::go(int input)
{
    if (isFree(input))
        square[input-1] = mark;
    else
    {
        cout<<"Invalid move! Try again!\n"<<endl;
        turn--;
        cin.ignore();
    }
}



bool Game::checkwin()
{
    if (   (square[0] == square[1] && square[1] == square[2])
        || (square[0] == square[3] && square[3] == square[6])
        || (square[0] == square[4] && square[4] == square[8])

        || (square[1] == square[4] && square[4] == square[7])

        || (square[2] == square[5] && square[5] == square[8])
        || (square[2] == square[4] && square[4] == square[6])

        || (square[3] == square[4] && square[4] == square[5])

        || (square[6] == square[7] && square[7] == square[8])
        )
        return true;

    else return false;
}


bool Game::isFull()
{
    if ( square[0] != '1' && square[1] != '2' && square[2] != '3'
        && square[3] != '4' && square[4] != '5' && square[5] != '6'
        && square[6] != '7' && square[7] != '8' && square[8] != '9'
        && !checkwin() )
        return true;

    else return false;
}


void Game::reset()
{
    for(int i=0; i<9; i++)
    {
        square[i] =(i+49);
    }
}



int Game::getMoveByAI()
{
    if (possWin()!=-1)
    {
        m= possWin();
    }
    else
    {
        do
        {
            m= rand()%10 ;
            // random move
        }
        while ( !isFree(m) );
    }
    return m;
}

bool Game::isFree(int input)
{
    if (square[input-1] == (input+48)) //48= '0' in ASCII. Compare the value of input with the corresponding element in the array
        return true;                    //by converting the input from the int type to the char type.
    
    else return false;
}



int Game::possWin()
{

     if (square[0] == square[1] && square[2]=='3') return 3;
     if (square[0] == square[2] && square[1]=='2') return 2;
     if (square[1] == square[2] && square[0]=='1') return 1;


     if (square[0] == square[3] && square[6]=='7') return 7;
     if (square[0] == square[6] && square[3]=='4') return 4;
     if (square[3] == square[6] && square[0]=='1') return 1;

     if (square[0] == square[4] && square[8]=='9') return 9;
     if (square[0] == square[8] && square[4]=='5') return 5;
     if (square[4] == square[8] && square[0]=='1') return 1;

     if (square[1] == square[4] && square[7]=='8') return 8;
     if (square[1] == square[7] && square[4]=='5') return 5;
     if (square[4] == square[7] && square[1]=='2') return 2;

     if (square[2] == square[5] && square[8]=='9') return 9;
     if (square[2] == square[8] && square[5]=='6') return 6;
     if (square[5] == square[8] && square[2]=='3') return 3;

     if (square[2] == square[4] && square[6]=='7') return 7;
     if (square[2] == square[6] && square[4]=='5') return 5;
     if (square[4] == square[6] && square[2]=='3') return 3;

     if (square[3] == square[4] && square[5]=='6') return 6;
     if (square[3] == square[5] && square[4]=='5') return 5;
     if (square[4] == square[5] && square[3]=='4') return 4;

     if (square[6] == square[7] && square[8]=='9') return 9;
     if (square[6] == square[8] && square[7]=='8') return 8;
     if (square[7] == square[8] && square[6]=='7') return 7;

     else return -1;
}
